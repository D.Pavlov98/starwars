async function getPlanets(page = 1, pageSize) {
    const numberPages = pageSize / 10
    const firstPageNumber = 1 + (page - 1)*numberPages;
    let promises = [];

    for (let i = 0; i < numberPages; i++){
        promises.push(fetch(`https://swapi.dev/api/planets/?page=${firstPageNumber+i}`))
    }
    let promises1 = [];
    await Promise.all(promises).then(values => {
        values.forEach(value => promises1.push(value.json()))
    })
    let res_obj = {total: 0, entities: []};
    await Promise.all(promises1).then(values=> {
        for(let i = 0; i < values.length;i++)
        {
            res_obj.total = values[i].count;
            res_obj.entities = res_obj.entities.concat(values[i].results);
        }
    })
    return res_obj;
}
function getPlanet(url) {
    return fetch(url).then(res => res.json())
}
(async () => {
    const PAGE_SIZE = 10;
    const planets = await getPlanets(1, PAGE_SIZE);
    renderPage(planets.entities);
    renderPaginator(planets.total, PAGE_SIZE);
    addPaginatorListener();
    handlePageSize();
    handleMoreInf();
})()

function renderCard(planetInfo) {
    const items = ['diameter', 'population', 'gravity', 'terrain', 'climate']
        .map(key => `<li>${key}: ${planetInfo[key]}</li>`).join('');

    return `
    <div class="card">
    <div class="card-body">
    <h5 class="card-title">${planetInfo.name}</h5>
    <ul>${items}</ul>
    <button type="button" data-link = "${planetInfo.url}" class="btn btn-primary js-open-modal" data-bs-toggle="modal" data-bs-target="#exampleModal">More information</button>
    </div>
    </div>
    `;
}

function renderPage(planets) {
    const cards = planets.map(planet => renderCard(planet)).join('');
    document.querySelector('.js-cards').innerHTML = cards;
    handleMoreInf()
}

let countPages = 0

function renderPaginator(total, pageSize) {
    countPages = Math.round(total / pageSize);
    let items = '<li class="page-item active"><a class="page-link" href="#" data-page="1">1</a></li>';
    for (let i = 1; i < countPages; i++) {
        items += `<li class="page-item"><a class="page-link" href="#" data-page="${i + 1}">${i + 1}</a></li>`
    }
    const disabledNext = (countPages === 1) ? 'disabled': ''

    const paginatorHtml = `
    <ul class="pagination">
    <li class="page-item previous disabled"><a class="page-link" data-page="prev" href="#">Previous</a></li>
    ${items}
    <li class="page-item next ${disabledNext}"><a class="page-link" data-page="next" href="#">Next</a></li>
    </ul>
    `;

    document.querySelector('.js-pagination').innerHTML = paginatorHtml;
}

function addPaginatorListener() {
    const paginatorItems = document.querySelectorAll('.js-pagination');
    paginatorItems.forEach(paginatorItem => paginatorItem.
    addEventListener('click', async event => {
        event.preventDefault();
        if (event.target.classList.contains('disabled') || !event.target.classList.contains('page-link')) {
            return
        }
        let pageNumber = event.target.getAttribute('data-page');
        if (pageNumber === 'prev') {
            document.querySelector('.next').classList.remove('disabled')
            const activePage = document.querySelector('.active');
            const  activePageRef = activePage.querySelector('.page-link')
            const activePageNum = activePageRef.getAttribute('data-page');
            let num = Number(activePageNum);
            pageNumber = String(--num);

            if (pageNumber === '1'){
                let prev = document.querySelector('.previous');
                prev.classList.add('disabled');
            }
        }
        else if (pageNumber === 'next') {
            document.querySelector('.previous').classList.remove('disabled');
            const activePage = document.querySelector('.active');
            const activePageRef = activePage.querySelector('.page-link')
            const activePageNum = activePageRef.getAttribute('data-page');
            let num = Number(activePageNum);
            pageNumber = String(++num);

            if (pageNumber === String(countPages)) {
                event.target.parentNode.classList.add('disabled');
            }
        }

        else if (pageNumber ==='1')
        {
            document.querySelector('.next').classList.remove('disabled');
            document.querySelector('.previous').classList.add('disabled');
        }
        else if(pageNumber === String(countPages))
        {
            document.querySelector('.previous').classList.remove('disabled');
            document.querySelector('.next').classList.add('disabled');
        }
        else {
            const paginatorItemPrev = document.querySelector('[data-page="prev"]');
            paginatorItemPrev.parentElement.classList.remove('disabled');
        }
        document.querySelectorAll('.page-item').forEach(itemEl => {
            itemEl.classList.remove('active');
            const itemRef = itemEl.querySelector('.page-link');
            const pageNum = itemRef.getAttribute('data-page');
            if (pageNum === pageNumber) {
                itemEl.classList.add('active');
            }
        });
        const currentPageSize = document.querySelector('.js-select');
        const planets = await getPlanets(pageNumber, +currentPageSize.value);
        renderPage(planets.entities);
        handleMoreInf()
    })
)
}
function  resetPagination(){
    document.querySelectorAll('.page-item').forEach(itemEl => {
        itemEl.classList.remove('active');
        document.querySelectorAll('.page-item')[1].classList.add('active');
    } )
}


// /*1. Список фильмов (films)
// - Номер эпизода (episode_id)
// - Название (title)
// - Дата выхода (release_date)
// 2. Список персонажей (residents)
// - Имя (name)
// - Пол (gender)
// - День рождения (birth_year)
// - Наименование родного мира (homeworld -> name)*/

async function getDataForModal(planetUrl) {

    const planet = await getPlanet(planetUrl);
    const films = await Promise.all(planet.films.map(filmsUrl => fetch(filmsUrl).then(res => res.json())));
    let residents = await Promise.all(planet.residents.map(residentUrl => fetch(residentUrl).then(res => res.json())));
    const nameHomeworldArr = await Promise.all(residents.map(resident => fetch(resident.homeworld).then(res => res.json())))

    residents = residents.map( function (elm, ind){
        elm.homeworld = nameHomeworldArr[ind].name;
        return elm
    })

    return {planet: planet, films: films, residents: residents}
}
function handlePageSize(){
    const selectEl = document.querySelector('.pagination-page-size select');
    selectEl.addEventListener('change', async event => {
        const pageSize = +event.target.value;
        const planets = await getPlanets(1, pageSize);
        renderPage(planets.entities);
        renderPaginator( 60, pageSize)
        resetPagination();
        handleMoreInf()
    })
}

function handleMoreInf(){
    const modalBody = document.querySelector('.js-modal-body');
    modalBody.innerHTML = '<div class="spinner-border js-spinner" role="status">\n' +
        '            <span class="visually-hidden">Loading...</span>\n' +
        '          </div>';

    document.querySelectorAll('.js-open-modal').forEach(btnMoreInf => btnMoreInf.addEventListener('click', async event => {
        const dataUrl = event.target.getAttribute('data-link');
    event.preventDefault();
    if (!event.target.classList.contains('js-open-modal')) {
        return;
        }
        const filmInfo = await getDataForModal(dataUrl);

        document.querySelector('.js-modal-title').innerHTML = filmInfo.planet.name
        const planetItems = ['diameter', 'population', 'gravity', 'terrain', 'climate']
            .map(key => `<li>${key}: ${filmInfo.planet[key]}</li>`).join('');
        let items = '';
        filmInfo.films.forEach(film => items += ['episode_id', 'title', 'release_date']
            .map(key => `<li>${key}: ${film[key]}</li>`).join(''))
        let heroes = '';
        filmInfo.residents.forEach(film => heroes += ['name', 'gender', 'birth_year', 'homeworld']
            .map(key => `<li>${key}: ${film[key]}</li>`).join(''));

        modalBody.innerHTML = `<ul>${planetItems}${items}${heroes}</ul>`;

    }))
}

